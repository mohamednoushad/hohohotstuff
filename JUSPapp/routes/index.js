var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', {
    title: 'Express'
  });
});

router.get('/exchange', function (req, res, next) {
  res.render('exchange', {
    title: 'Express'
  });
});

module.exports = router;