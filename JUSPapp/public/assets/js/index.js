var ethPrice;
var currency = "USD";
var percentageForYear = 60;
var PercentageForSixMonth = percentageForYear / 2;
var unixTimestampInADay = 86400000;

const abi = [{
    "constant": false,
    "inputs": [{
        "name": "spender",
        "type": "address"
    }, {
        "name": "amount",
        "type": "uint256"
    }],
    "name": "approve",
    "outputs": [{
        "name": "",
        "type": "bool"
    }],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
}, {
    "constant": false,
    "inputs": [],
    "name": "createStake",
    "outputs": [{
        "name": "success",
        "type": "bool"
    }],
    "payable": true,
    "stateMutability": "payable",
    "type": "function"
}, {
    "constant": false,
    "inputs": [{
        "name": "spender",
        "type": "address"
    }, {
        "name": "subtractedValue",
        "type": "uint256"
    }],
    "name": "decreaseAllowance",
    "outputs": [{
        "name": "",
        "type": "bool"
    }],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
}, {
    "constant": false,
    "inputs": [{
        "name": "spender",
        "type": "address"
    }, {
        "name": "addedValue",
        "type": "uint256"
    }],
    "name": "increaseAllowance",
    "outputs": [{
        "name": "",
        "type": "bool"
    }],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
}, {
    "constant": false,
    "inputs": [{
        "name": "recipient",
        "type": "address"
    }, {
        "name": "amount",
        "type": "uint256"
    }],
    "name": "transfer",
    "outputs": [{
        "name": "",
        "type": "bool"
    }],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
}, {
    "constant": false,
    "inputs": [{
        "name": "sender",
        "type": "address"
    }, {
        "name": "recipient",
        "type": "address"
    }, {
        "name": "amount",
        "type": "uint256"
    }],
    "name": "transferFrom",
    "outputs": [{
        "name": "",
        "type": "bool"
    }],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
}, {
    "constant": false,
    "inputs": [],
    "name": "withdrawReward",
    "outputs": [{
        "name": "",
        "type": "uint256"
    }],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
}, {
    "constant": false,
    "inputs": [{
        "name": "_now",
        "type": "uint256"
    }],
    "name": "withdrawRewardLater",
    "outputs": [{
        "name": "",
        "type": "uint256"
    }],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
}, {
    "constant": false,
    "inputs": [],
    "name": "withdrawStake",
    "outputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
}, {
    "constant": false,
    "inputs": [{
        "name": "_now",
        "type": "uint256"
    }],
    "name": "withdrawStakeLater",
    "outputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
}, {
    "inputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "constructor"
}, {
    "anonymous": false,
    "inputs": [{
        "indexed": true,
        "name": "from",
        "type": "address"
    }, {
        "indexed": true,
        "name": "to",
        "type": "address"
    }, {
        "indexed": false,
        "name": "value",
        "type": "uint256"
    }],
    "name": "Transfer",
    "type": "event"
}, {
    "anonymous": false,
    "inputs": [{
        "indexed": true,
        "name": "owner",
        "type": "address"
    }, {
        "indexed": true,
        "name": "spender",
        "type": "address"
    }, {
        "indexed": false,
        "name": "value",
        "type": "uint256"
    }],
    "name": "Approval",
    "type": "event"
}, {
    "constant": true,
    "inputs": [{
        "name": "owner",
        "type": "address"
    }, {
        "name": "spender",
        "type": "address"
    }],
    "name": "allowance",
    "outputs": [{
        "name": "",
        "type": "uint256"
    }],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
}, {
    "constant": true,
    "inputs": [{
        "name": "_now",
        "type": "uint256"
    }],
    "name": "approximateRewardForALaterTime",
    "outputs": [{
        "name": "",
        "type": "uint256"
    }],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
}, {
    "constant": true,
    "inputs": [{
        "name": "account",
        "type": "address"
    }],
    "name": "balanceOf",
    "outputs": [{
        "name": "",
        "type": "uint256"
    }],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
}, {
    "constant": true,
    "inputs": [],
    "name": "calculateRewardForNow",
    "outputs": [{
        "name": "",
        "type": "uint256"
    }],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
}, {
    "constant": true,
    "inputs": [],
    "name": "decimals",
    "outputs": [{
        "name": "",
        "type": "uint8"
    }],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
}, {
    "constant": true,
    "inputs": [{
        "name": "_now",
        "type": "uint256"
    }],
    "name": "getCoinAge",
    "outputs": [{
        "name": "_coinAge",
        "type": "uint256"
    }],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
}, {
    "constant": true,
    "inputs": [],
    "name": "getUserStakedDetails",
    "outputs": [{
        "name": "",
        "type": "uint256"
    }, {
        "name": "",
        "type": "uint64"
    }, {
        "name": "",
        "type": "bool"
    }],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
}, {
    "constant": true,
    "inputs": [{
        "name": "_customerAddress",
        "type": "address"
    }],
    "name": "hasUserAlreadyStaked",
    "outputs": [{
        "name": "",
        "type": "bool"
    }],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
}, {
    "constant": true,
    "inputs": [],
    "name": "maxMintProofOfStake",
    "outputs": [{
        "name": "",
        "type": "uint256"
    }],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
}, {
    "constant": true,
    "inputs": [],
    "name": "name",
    "outputs": [{
        "name": "",
        "type": "string"
    }],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
}, {
    "constant": true,
    "inputs": [{
        "name": "",
        "type": "address"
    }],
    "name": "redeemedTokens",
    "outputs": [{
        "name": "",
        "type": "uint256"
    }],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
}, {
    "constant": true,
    "inputs": [],
    "name": "rewardsAvailableForWithdrawal",
    "outputs": [{
        "name": "",
        "type": "uint256"
    }],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
}, {
    "constant": true,
    "inputs": [{
        "name": "",
        "type": "address"
    }],
    "name": "stakes",
    "outputs": [{
        "name": "",
        "type": "uint256"
    }],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
}, {
    "constant": true,
    "inputs": [],
    "name": "symbol",
    "outputs": [{
        "name": "",
        "type": "string"
    }],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
}, {
    "constant": true,
    "inputs": [],
    "name": "totalEthereumBalance",
    "outputs": [{
        "name": "",
        "type": "uint256"
    }],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
}, {
    "constant": true,
    "inputs": [],
    "name": "totalSupply",
    "outputs": [{
        "name": "",
        "type": "uint256"
    }],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
}]

const contractAddress = "0xa10b09949d548f06c883c551a25d96151caa3102";

function convertWeiToEth(e) {
    return e / 1e18
}


function convertEthToWei(e) {
    return 1e18 * e
}

function formatTime(timestamp) {
    var currentTime = new Date().getTime();
    timestamp = timestamp * 1000;
    // timestamp = Number(timestamp * 10 ** -6);
    var seconds = Math.floor((currentTime - timestamp) / 1000);
    var minutes = Math.floor(seconds / 60);
    var hours = Math.floor(minutes / 60);
    var days = Math.floor(hours / 24);
    var returnValue = '';

    // let daysInLanguage = my8n('days');
    let daysInLanguage = " " + "days";

    returnValue += days > 0 ? days + daysInLanguage : '';
    hours = hours - days * 24;

    // let hoursInLanguage = my8n('hours');
    let hoursInLanguage = " " + "hours";

    returnValue +=
        hours > 0 ? (returnValue !== '' ? ' ' : '') + hours + hoursInLanguage : '';
    if (returnValue.indexOf('days') === -1) {
        minutes = minutes - days * 24 * 60 - hours * 60;

        // let minsInLanguage = my8n('mins');
        let minsInLanguage = " " + "mins";

        returnValue +=
            minutes > 0 ?
            (returnValue !== '' ? ' ' : '') + minutes + minsInLanguage :
            '';
    }
    if (returnValue.indexOf('hours') === -1) {
        seconds = seconds - days * 24 * 60 * 60 - hours * 60 * 60 - minutes * 60;

        // let secondsInLanguage = my8n('secs');
        let secondsInLanguage = " " + "secs";

        returnValue +=
            seconds > 0 ?
            (returnValue !== '' ? ' ' : '') + seconds + secondsInLanguage :
            '';
    }
    return returnValue;
}


const App = {
    web3: null,
    account: null,
    meta: null,

    start: async function () {
        const {
            web3
        } = this;

        try {
            console.log(web3.eth.accounts);


            this.meta = new web3.eth.Contract(
                abi,
                contractAddress,
            );

            // get accounts
            const accounts = await web3.eth.getAccounts();
            this.account = accounts[0];

            await this.updateEthPrice();
            await this.refreshTable();

        } catch (error) {
            console.log(error);
            $('#msg').text(error);
            $('#exampleModal').modal('show');
        }
    },

    updateEthPrice: async function () {
        const result = await $.getJSON('https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&ids=ethereum');
        var eth = result[0];
        ethPrice = parseFloat(eth['current_price']);
    },

    refreshTable: async function () {

        web3.eth.getBalance(contractAddress, function (e, r) {
            $('.contract-balance').text(convertWeiToEth(r).toFixed(4) + " ETH");
            console.log("undo", ethPrice);
            $('.contract-balance-usd').text('(' + Number((convertWeiToEth(r) * ethPrice).toFixed(2)).toLocaleString() + ' ' + currency + ')');
        })

        const {
            totalSupply,
            balanceOf,
            getUserStakedDetails,
            redeemedTokens,
            calculateRewardForNow,
            rewardsAvailableForWithdrawal,
            approximateRewardForALaterTime
        } = this.meta.methods;

        const totalCoinsInSupply = await totalSupply().call();
        $('.contract-tokens').text(convertWeiToEth(totalCoinsInSupply));

        const userJUSPBalance = await balanceOf(this.account).call();
        $('.user-jusp-balance').text(convertWeiToEth(userJUSPBalance));

        $('#inputEth').bind("keypress keyup click", async function (e) {
            var number = $('#inputEth').val();
            var value = parseFloat(number);
            if (value === 0 || Number.isNaN(value)) {
                $('#deposit-hint').text("");
                return;
            }
            if (value > 0) {
                var tokensBackToRealNumber = ((PercentageForSixMonth / 100) * value) * 100;
                $('#deposit-hint').text("You will get an Interest of approximately " + tokensBackToRealNumber.toFixed(2) + " JUSP tokens after 6 Months!"); //Two decimal Token, Hence / 100
            }
        })

        const userStakedDetails = await getUserStakedDetails().call({
            from: this.account
        });

        console.log("user staked details", userStakedDetails);

        let userStakedEth = convertWeiToEth(userStakedDetails[0]);
        $("#ethStaked").text(userStakedEth);

        console.log("this1", userStakedDetails[0]);
        if (+(userStakedDetails[1]) == 0) {

            $("#age").text("No Staking");

        } else {

            let userStakedEthDate = formatTime(userStakedDetails[1]);
            console.log("this", userStakedEthDate);
            $("#age").text(userStakedEthDate);

        }


        //interest

        const userJUSPRewardsForNow = await calculateRewardForNow().call({
            from: this.account
        })
        $("#userEarnedJUSP").text(userJUSPRewardsForNow);


        const userRedeemedJUSP = await redeemedTokens(this.account).call();
        $("#userConsumedJUSP").text(userRedeemedJUSP);

        const rewardUserCanWithdraw = await rewardsAvailableForWithdrawal().call({
            from: this.account
        });
        console.log("JUSP available for Withdrawal", rewardUserCanWithdraw);
        $("#available4WithdrawalJUSP").text(rewardUserCanWithdraw);

        //approximate reward for after six months

        //get present time stamp
        // var date = new Date();
        // var timestamp = date.getTime();
        // // timestampStr = timestamp.toString();
        // timestampStr = timestamp.toString().slice(0, -3);
        // timestampStr = parseInt(timestampStr);
        // console.log(timestampStr);

        if (userStakedDetails[2] == true) {

            console.log("user has staking")

            // get present time stamp
            var date = new Date();
            console.log(date);
            var timestamp = date.getTime();
            console.log("timestamp", timestamp)
            var timeStampAfterSixMonths = timestamp + unixTimestampInADay * 30 * 6;
            console.log("after six months", timeStampAfterSixMonths);
            var dateAfterSixMonths = new Date(timeStampAfterSixMonths);
            console.log("date after six months", dateAfterSixMonths);
            $("#estimator-date").text("The amount of JUSP you will bw getting on " + dateAfterSixMonths + " is")
            //  timestampStr = timestamp.toString();
            timestampStrAfterSixMonth = timeStampAfterSixMonths.toString().slice(0, -3);
            timestampInNumberAfterSixMonth = parseInt(timestampStrAfterSixMonth);
            console.log("to check", timestampInNumberAfterSixMonth);
            const rewardAfterSixMonths = await approximateRewardForALaterTime(timestampInNumberAfterSixMonth).call({
                from: this.account
            });
            console.log("reward after six month", convertWeiToEth(rewardAfterSixMonths));
            var rewardsAvailableAfterSixMonth = Math.round(convertWeiToEth(rewardAfterSixMonths));
            console.log(rewardsAvailableAfterSixMonth);
            $("#approximated-jusp").text(rewardsAvailableAfterSixMonth);


        } else {

            console.log("user does not have staking");
            $("#approximated-jusp").text("User Has Not Staked to Estimate");

        }





    },

    stake: async function () {

        const {
            createStake,
            hasUserAlreadyStaked
        } = this.meta.methods;

        let amount = $('#inputEth').val().trim()

        if (amount <= 0 || !isFinite(amount) || amount === '') {

            $('#msg').text('Please Enter a Valid Amount');
            $('#exampleModal').modal('show');

        } else {

            let userAlreadyStaked = await hasUserAlreadyStaked(this.account).call();
            console.log("user stake cheythitundo", userAlreadyStaked);

            if (userAlreadyStaked) {

                $('#msg').text('You have already staked, Withdraw and Stake if you want to Stake Again');
                $('#exampleModal').modal('show');

            } else {

                await createStake().send({
                    from: this.account,
                    value: convertEthToWei(amount)
                });

                location.reload();

            }

        }

    },

    withdrawStakeAndRewards: async function () {

        const {
            withdrawStake,
            hasUserAlreadyStaked
        } = this.meta.methods;


        let userAlreadyStaked = await hasUserAlreadyStaked(this.account).call();
        console.log("user stake cheythitundo 2", userAlreadyStaked);

        if (userAlreadyStaked) {

            await withdrawStake().send({
                from: this.account
            });

            location.reload();

        } else {

            $('#msg').text('User has no staking to Withdraw');
            $('#exampleModal').modal('show');
        }

    },

    withdrawRewards: async function () {

        const {
            withdrawReward,
            rewardsAvailableForWithdrawal
        } = this.meta.methods;

        const rewardUserCanWithdraw = await rewardsAvailableForWithdrawal().call({
            from: this.account
        });
        console.log("JUSP available for Withdrawal", rewardUserCanWithdraw);


        if (rewardUserCanWithdraw > 0) {

            await withdrawReward().send({
                from: this.account
            });

            location.reload();

        } else {

            $('#msg').text('You do not have Earned JUSP to Withdraw');
            $('#exampleModal').modal('show');
        }

    },

    transferTokens: async function () {

        let address = $('#transfer-address').val();
        let amount = $('#transfer-tokens').val();

        if (web3.isAddress(address) && parseFloat(amount)) {

            var amountConverted = web3.toWei(amount);

            const {
                transfer
            } = this.meta.methods;

            await transfer(address, amountConverted).send({
                from: this.account
            });

            location.reload();

        } else {
            $('#msg').text('Invalid Address / Amount');
            $('#exampleModal').modal('show');
        }

    }

}

function openEtherscan() {
    window.open(
        'https://rinkeby.etherscan.io/token/' + contractAddress,
        '_blank'
    );
}




window.App = App;



$(document).ready(function () {

    if (window.ethereum) {

        App.web3 = new Web3(window.ethereum);

        window.ethereum.enable();

        // window.ethereum.on('accountsChanged', function (accounts) {
        //     location.reload();
        // })

        // console.log(web3.eth.accounts);
        // window.ethereum.on('accountsChanged', function (accounts) {
        //   location.reload();
        // })
        // window.ethereum.on('networkChanged', function (accounts) {
        //   location.reload();
        // })
    } else {
        console.warn(
            "No web3 detected. Falling back to http://127.0.0.1:8545. You should remove this fallback when you deploy live",
        );
        // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
        App.web3 = new Web3(
            new Web3.providers.HttpProvider("http://127.0.0.1:8545"),
        );
    }

    App.start();

});