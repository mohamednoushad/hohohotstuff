pragma solidity ^0.5.0;

// abstract contract Context {
//     function _msgSender() internal view virtual returns (address payable) {
//         return msg.sender;
//     }

//     function _msgData() internal view virtual returns (bytes memory) {
//         this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
//         return msg.data;
//     }
// }

interface IERC20 {

    function totalSupply() external view returns (uint256);
    function balanceOf(address account) external view returns (uint256);
    function transfer(address recipient, uint256 amount) external returns (bool);
    function allowance(address owner, address spender) external view returns (uint256);
    function approve(address spender, uint256 amount) external returns (bool);
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);

    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);
}

library SafeMath {
  
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c >= a, "SafeMath: addition overflow");

        return c;
    }

   
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return sub(a, b, "SafeMath: subtraction overflow");
    }

  
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b <= a, errorMessage);
        uint256 c = a - b;

        return c;
    }

   
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, "SafeMath: multiplication overflow");

        return c;
    }

 
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return div(a, b, "SafeMath: division by zero");
    }

  
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b > 0, errorMessage);
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn't hold

        return c;
    }

  
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        return mod(a, b, "SafeMath: modulo by zero");
    }

 
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b != 0, errorMessage);
        return a % b;
    }
}






contract JUSP is IERC20 {
    using SafeMath for uint256;


    mapping (address => uint256) private _balances;

    mapping (address => mapping (address => uint256)) private _allowances;

    uint256 private _totalSupply;

    string private _name;
    string private _symbol;
    uint8 private _decimals;

    /**
     * @dev Sets the values for {name} and {symbol}, initializes {decimals} with
     * a default value of 18.
     *
     * To select a different value for {decimals}, use {_setupDecimals}.
     *
     * All three of these values are immutable: they can only be set once during
     * construction.
     */
    constructor () public {
        _name = "jusstoken";
        _symbol = "JUSP";
        _decimals = 18;
        _mint(0xC641AD8Dd7875608F3fA4372aB559D64992ee356, 1000000e18);
    }

    /**
     * @dev Returns the name of the token.
     */
    function name() public view returns (string memory) {
        return _name;
    }

    /**
     * @dev Returns the symbol of the token, usually a shorter version of the
     * name.
     */
    function symbol() public view returns (string memory) {
        return _symbol;
    }


    function decimals() public view returns (uint8) {
        return _decimals;
    }


    function totalSupply() public view returns (uint256) {
        return _totalSupply;
    }

 
    function balanceOf(address account) public view returns (uint256) {
        return _balances[account];
    }


    function transfer(address recipient, uint256 amount) public returns (bool) {
        _transfer(msg.sender, recipient, amount);
        return true;
    }


    function allowance(address owner, address spender) public view  returns (uint256) {
        return _allowances[owner][spender];
    }


    function approve(address spender, uint256 amount) public returns (bool) {
        _approve(msg.sender, spender, amount);
        return true;
    }


    function transferFrom(address sender, address recipient, uint256 amount) public  returns (bool) {
        _transfer(sender, recipient, amount);
        _approve(sender, msg.sender, _allowances[sender][msg.sender].sub(amount, "ERC20: transfer amount exceeds allowance"));
        return true;
    }


    function increaseAllowance(address spender, uint256 addedValue) public  returns (bool) {
        _approve(msg.sender, spender, _allowances[msg.sender][spender].add(addedValue));
        return true;
    }


    function decreaseAllowance(address spender, uint256 subtractedValue) public returns (bool) {
        _approve(msg.sender, spender, _allowances[msg.sender][spender].sub(subtractedValue, "ERC20: decreased allowance below zero"));
        return true;
    }

 
    function _transfer(address sender, address recipient, uint256 amount) internal  {
        require(sender != address(0), "ERC20: transfer from the zero address");
        require(recipient != address(0), "ERC20: transfer to the zero address");


        _balances[sender] = _balances[sender].sub(amount, "ERC20: transfer amount exceeds balance");
        _balances[recipient] = _balances[recipient].add(amount);
        emit Transfer(sender, recipient, amount);
    }


    function _mint(address account, uint256 amount) internal {
        require(account != address(0), "ERC20: mint to the zero address");

         _totalSupply = _totalSupply.add(amount);
        _balances[account] = _balances[account].add(amount);
        emit Transfer(address(0), account, amount);
    }


    function _burn(address account, uint256 amount) internal  {
        require(account != address(0), "ERC20: burn from the zero address");

        _balances[account] = _balances[account].sub(amount, "ERC20: burn amount exceeds balance");
        _totalSupply = _totalSupply.sub(amount);
        emit Transfer(account, address(0), amount);
    }


    function _approve(address owner, address spender, uint256 amount) internal {
        require(owner != address(0), "ERC20: approve from the zero address");
        require(spender != address(0), "ERC20: approve to the zero address");

        _allowances[owner][spender] = amount;
        emit Approval(owner, spender, amount);
    }

 
    function _setupDecimals(uint8 decimals_) internal {
        _decimals = decimals_;
    }

  
 
    
    //staking logics
    
    modifier userWhoHasNotStakedBefore() {
        require(!stakingStructs[msg.sender].hasStaked);
        _;
    }
    
    struct stakingStruct{
        uint256 amount;
        uint64 startTime;
        bool hasStaked;
    }
    
    mapping(address => uint256) public stakes;
    
    mapping(address => stakingStruct) stakingStructs;
    
    uint public maxMintProofOfStake = 10**17; // default 10% annual interest
    
    function createStake() payable userWhoHasNotStakedBefore() public returns(bool success) {
        
        address _customerAddress = msg.sender;
        uint256 _incomingEthereum = msg.value;
        
        // stakingStructs[_customerAddress].amount = _incomingEthereum;
        
        stakingStructs[_customerAddress].amount = _incomingEthereum;
        stakingStructs[_customerAddress].startTime = uint64(now);
        stakingStructs[_customerAddress].hasStaked = true;
        
        return true;
        
    }
    
    function totalEthereumBalance() public view returns(uint)  {
        return address(this).balance;
    }
    
    function getUserStakedDetails() public view returns(uint256,uint64,bool) {
        
        address _customerAddress = msg.sender;
        return (stakingStructs[_customerAddress].amount,
        stakingStructs[_customerAddress].startTime,
        stakingStructs[_customerAddress].hasStaked);
        
    }
    
     function getProofOfStakeReward() public view returns (uint) {
         

        uint _now = now;
        uint _coinAge = getCoinAge(_now);
        if(_coinAge <= 0) return 0;

        uint interest = maxMintProofOfStake;
        // Due to the high interest rate for the first two years, compounding should be taken into account.
        // Effective annual interest rate = (1 + (nominal rate / number of compounding periods)) ^ (number of compounding periods) - 1
     
            // 1st year effective annual interest rate is 100% when we select the stakeMaxAge (90 days) as the compounding period.
            interest = (770 * maxMintProofOfStake).div(100);
    

        return (_coinAge * interest).div(365 * (10**18));
    }

    function getCoinAge(uint _now) public view returns (uint _coinAge) {
        
        address _address = msg.sender;
        
        if(!stakingStructs[_address].hasStaked) return 0;

      
            // if( _now < uint(transferIns[_address][i].time).add(stakeMinAge) ) continue;

            uint nCoinSeconds = _now.sub(uint(stakingStructs[_address].startTime));
  

            _coinAge = _coinAge.add(uint(stakingStructs[_address].amount) * nCoinSeconds.div(1 days));
     
    }
    
     function approximateRewardForALaterTime(uint _now) public view returns (uint) {
         

        uint _coinAge = getCoinAge(_now);
        if(_coinAge <= 0) return 0;

        uint interest = maxMintProofOfStake;
        // Due to the high interest rate for the first two years, compounding should be taken into account.
        // Effective annual interest rate = (1 + (nominal rate / number of compounding periods)) ^ (number of compounding periods) - 1
     
            // 1st year effective annual interest rate is 100% when we select the stakeMaxAge (90 days) as the compounding period.
            interest = (770 * maxMintProofOfStake).div(100);
    

        return (_coinAge * interest).div(365 * (10**18));
    }
    
    // function calculateReward() public view returns(uint){
        
    //     address _customerAddress = msg.sender;
        
    //     uint256 userStakedEth = stakingStructs[_customerAddress].amount;
        
    //     uint64 coinAge = uint64(now) - stakingStructs[_customerAddress].startTime; 
        
        
    // }
    
    function getPresentTime() public view returns(uint) {
        
        return uint64(now);
        
    }
    
    // function createStake(uint256 _stake)
    //     public
    // {
    //     _burn(msg.sender, _stake);
    //     if(stakes[msg.sender] == 0) addStakeholder(msg.sender);
    //     stakes[msg.sender] = stakes[msg.sender].add(_stake);
    // }
    
    
}
